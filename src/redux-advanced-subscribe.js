'use strict';

import assign from 'object-assign';
import findIndex from 'array-find-index';

const { isArray } = Array;

const extractKeys = (o) => (r, v) => (r.push(o[v]), r);

export default (store) => {
  const { dispatch, getState } = store;
  const evtSubscribers = [], keySubscribers = [], stateSubscribers = [];
  let interrupter = false, mutex = false;
  const injectedDispatch = (evt) => {
    if (interrupter) return;
    const lastState = getState();
    dispatch(evt);
    const newState = getState();
    if (!mutex) {
      evtSubscribers.forEach((v) => {
        if (v.evt === evt.type) v.fn(lastState, newState);
      });
      stateSubscribers.forEach((v) => {
        v.fn(lastState, newState);
      });
      keySubscribers.forEach((v) => {
        if (isArray(v.key)) {
          if (v.key.reduce((r, v) => {
            if (r) return r;
            if (newState[v] !== lastState[v]) return true;
            return false;
          }, false)) {
            v.fn(lastState, newState);
          }
        } else if (newState[v.key] !== lastState[v.key]) {
          v.fn(lastState[v.key], newState[v.key]);
        }
      });
    }
    return evt;
  };
  injectedDispatch.interrupt = () => (interrupter = true);
  injectedDispatch.setMutex = () => (mutex = true);
  injectedDispatch.releaseMutex = () => (mutex = false);
  injectedDispatch.getMutex = () => mutex;
  injectedDispatch.mutexDispatch = (evt) => {
    injectedDispatch.setMutex();
    injectedDispatch(evt);
    injectedDispatch.releaseMutex();
  };
  const subscribeToEvent = (evt, fn) => {
    const o = { evt, fn };
    evtSubscribers.push(o);
    return () => {
      const i = findIndex(evtSubscribers, (v) => v === o);
      if (~i) {
        evtSubscribers.splice(i, 1);
        return true;
      }
      return false;
    };
  };
  const subscribeToEventOnce = (evt, fn) => {
    const unsubscribe = subscribeToEvent(evt, (lastState, newState) => {
      unsubscribe();
      fn(lastState, newState);
    });
  };
  const subscribeToState = (fn) => {
    const o = { fn };
    stateSubscribers.push(o);
    return () => {
      const i = findIndex(stateSubscribers, (v) => v === o);
      if (~i) {
        stateSubscribers.splice(i, 1);
        return true;
      }
      return false;
    };
  };
  const subscribeToStateOnce = (fn) => {
    const unsubscribe = subscribeToState((lastState, newState) => {
      unsubscribe();
      fn(lastState, newState);
    });
  };
  const subscribeToKey = (key, fn) => {
    const o = { key, fn };
    keySubscribers.push({ key, fn });
    return () => {
      const i = findIndex(keySubscribers, (v) => v === o);
      if (~i) {
        keySubscribers.splice(i, 1);
        return true;
      }
      return false;
    };
  };
  const subscribeToKeyOnce = (key, fn) => {
    const unsubscribe = subscribeToKey(key, (lastState, newState) => {
      unsubscribe();
      fn(lastState, newState);
    });
  };
  return assign({}, store, {
    dispatch: injectedDispatch,
    subscribeToState,
    subscribeToStateOnce,
    subscribeToEvent,
    subscribeToEventOnce,
    subscribeToKey,
    subscribeToKeyOnce
  });
};
