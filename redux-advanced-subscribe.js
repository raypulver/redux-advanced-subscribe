(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("injectSubscribe", [], factory);
	else if(typeof exports === 'object')
		exports["injectSubscribe"] = factory();
	else
		root["injectSubscribe"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _objectAssign = __webpack_require__(1);
	
	var _objectAssign2 = _interopRequireDefault(_objectAssign);
	
	var _arrayFindIndex = __webpack_require__(2);
	
	var _arrayFindIndex2 = _interopRequireDefault(_arrayFindIndex);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var isArray = Array.isArray;
	
	
	var extractKeys = function extractKeys(o) {
	  return function (r, v) {
	    return r.push(o[v]), r;
	  };
	};
	
	exports.default = function (store) {
	  var dispatch = store.dispatch,
	      getState = store.getState;
	
	  var evtSubscribers = [],
	      keySubscribers = [],
	      stateSubscribers = [];
	  var interrupter = false,
	      mutex = false;
	  var injectedDispatch = function injectedDispatch(evt) {
	    if (interrupter) return;
	    var lastState = getState();
	    dispatch(evt);
	    var newState = getState();
	    if (!mutex) {
	      evtSubscribers.forEach(function (v) {
	        if (v.evt === evt.type) v.fn(lastState, newState);
	      });
	      stateSubscribers.forEach(function (v) {
	        v.fn(lastState, newState);
	      });
	      keySubscribers.forEach(function (v) {
	        if (isArray(v.key)) {
	          if (v.key.reduce(function (r, v) {
	            if (r) return r;
	            if (newState[v] !== lastState[v]) return true;
	            return false;
	          }, false)) {
	            v.fn(lastState, newState);
	          }
	        } else if (newState[v.key] !== lastState[v.key]) {
	          v.fn(lastState[v.key], newState[v.key]);
	        }
	      });
	    }
	    return evt;
	  };
	  injectedDispatch.interrupt = function () {
	    return interrupter = true;
	  };
	  injectedDispatch.setMutex = function () {
	    return mutex = true;
	  };
	  injectedDispatch.releaseMutex = function () {
	    return mutex = false;
	  };
	  injectedDispatch.getMutex = function () {
	    return mutex;
	  };
	  injectedDispatch.mutexDispatch = function (evt) {
	    injectedDispatch.setMutex();
	    injectedDispatch(evt);
	    injectedDispatch.releaseMutex();
	  };
	  var subscribeToEvent = function subscribeToEvent(evt, fn) {
	    var o = { evt: evt, fn: fn };
	    evtSubscribers.push(o);
	    return function () {
	      var i = (0, _arrayFindIndex2.default)(evtSubscribers, function (v) {
	        return v === o;
	      });
	      if (~i) {
	        evtSubscribers.splice(i, 1);
	        return true;
	      }
	      return false;
	    };
	  };
	  var subscribeToEventOnce = function subscribeToEventOnce(evt, fn) {
	    var unsubscribe = subscribeToEvent(evt, function (lastState, newState) {
	      unsubscribe();
	      fn(lastState, newState);
	    });
	  };
	  var subscribeToState = function subscribeToState(fn) {
	    var o = { fn: fn };
	    stateSubscribers.push(o);
	    return function () {
	      var i = (0, _arrayFindIndex2.default)(stateSubscribers, function (v) {
	        return v === o;
	      });
	      if (~i) {
	        stateSubscribers.splice(i, 1);
	        return true;
	      }
	      return false;
	    };
	  };
	  var subscribeToStateOnce = function subscribeToStateOnce(fn) {
	    var unsubscribe = subscribeToState(function (lastState, newState) {
	      unsubscribe();
	      fn(lastState, newState);
	    });
	  };
	  var subscribeToKey = function subscribeToKey(key, fn) {
	    var o = { key: key, fn: fn };
	    keySubscribers.push({ key: key, fn: fn });
	    return function () {
	      var i = (0, _arrayFindIndex2.default)(keySubscribers, function (v) {
	        return v === o;
	      });
	      if (~i) {
	        keySubscribers.splice(i, 1);
	        return true;
	      }
	      return false;
	    };
	  };
	  var subscribeToKeyOnce = function subscribeToKeyOnce(key, fn) {
	    var unsubscribe = subscribeToKey(key, function (lastState, newState) {
	      unsubscribe();
	      fn(lastState, newState);
	    });
	  };
	  return (0, _objectAssign2.default)({}, store, {
	    dispatch: injectedDispatch,
	    subscribeToState: subscribeToState,
	    subscribeToStateOnce: subscribeToStateOnce,
	    subscribeToEvent: subscribeToEvent,
	    subscribeToEventOnce: subscribeToEventOnce,
	    subscribeToKey: subscribeToKey,
	    subscribeToKeyOnce: subscribeToKeyOnce
	  });
	};
	
	module.exports = exports['default'];

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';
	/* eslint-disable no-unused-vars */
	var hasOwnProperty = Object.prototype.hasOwnProperty;
	var propIsEnumerable = Object.prototype.propertyIsEnumerable;
	
	function toObject(val) {
		if (val === null || val === undefined) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}
	
		return Object(val);
	}
	
	function shouldUseNative() {
		try {
			if (!Object.assign) {
				return false;
			}
	
			// Detect buggy property enumeration order in older V8 versions.
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=4118
			var test1 = new String('abc');  // eslint-disable-line
			test1[5] = 'de';
			if (Object.getOwnPropertyNames(test1)[0] === '5') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test2 = {};
			for (var i = 0; i < 10; i++) {
				test2['_' + String.fromCharCode(i)] = i;
			}
			var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
				return test2[n];
			});
			if (order2.join('') !== '0123456789') {
				return false;
			}
	
			// https://bugs.chromium.org/p/v8/issues/detail?id=3056
			var test3 = {};
			'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
				test3[letter] = letter;
			});
			if (Object.keys(Object.assign({}, test3)).join('') !==
					'abcdefghijklmnopqrst') {
				return false;
			}
	
			return true;
		} catch (e) {
			// We don't expect any of the above to throw, but better to be safe.
			return false;
		}
	}
	
	module.exports = shouldUseNative() ? Object.assign : function (target, source) {
		var from;
		var to = toObject(target);
		var symbols;
	
		for (var s = 1; s < arguments.length; s++) {
			from = Object(arguments[s]);
	
			for (var key in from) {
				if (hasOwnProperty.call(from, key)) {
					to[key] = from[key];
				}
			}
	
			if (Object.getOwnPropertySymbols) {
				symbols = Object.getOwnPropertySymbols(from);
				for (var i = 0; i < symbols.length; i++) {
					if (propIsEnumerable.call(from, symbols[i])) {
						to[symbols[i]] = from[symbols[i]];
					}
				}
			}
		}
	
		return to;
	};


/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';
	module.exports = function (arr, predicate, ctx) {
		if (typeof Array.prototype.findIndex === 'function') {
			return arr.findIndex(predicate, ctx);
		}
	
		if (typeof predicate !== 'function') {
			throw new TypeError('predicate must be a function');
		}
	
		var list = Object(arr);
		var len = list.length;
	
		if (len === 0) {
			return -1;
		}
	
		for (var i = 0; i < len; i++) {
			if (predicate.call(ctx, list[i], i, list)) {
				return i;
			}
		}
	
		return -1;
	};


/***/ }
/******/ ])
});
;
//# sourceMappingURL=redux-advanced-subscribe.js.map