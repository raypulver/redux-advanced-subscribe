'use strict';

var path = require('path'),
    join = path.join,
    forOwn = require('lodash/forOwn'),
    deepAssign = require('deep-assign'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    fs = require('fs'),
    webpack = require('webpack'),
    ClosureCompilerPlugin = require('webpack-closure-compiler'),
    writeFileSync = fs.writeFileSync,
    tasks = gulp.tasks,
    src = gulp.src.bind(gulp),
    dest = gulp.src.bind(gulp),
    task = gulp.task.bind(gulp);

var baseWebpackCfg = {
  entry: join(__dirname, 'src', 'redux-advanced-subscribe.js'),
  output: {
    path: __dirname,
    library: 'injectSubscribe',
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  devtool: 'source-map',
  module: {
    loaders: [{
      test: /(?:\.js$)/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'stage-2'],
        plugins: ['add-module-exports']
      }
    }]
  }
};

task('default', ['build']);

task('build:tasks', function () {
  var packageJSON = require('./package');
  packageJSON.scripts = {};
  forOwn(tasks, function (val, key) {
    if (key !== 'default') packageJSON.scripts[key] = 'node ' + join('node_modules', '.bin', 'gulp') + ' ' + key;
  });
  writeFileSync('./package.json', JSON.stringify(packageJSON, null, 1));;
});

task('build', ['build:dev', 'build:min']);

function doWebpack(cfg, next) {
  webpack(cfg, function (err, stats) {
    if (err) return next(new gutil.PluginError('[webpack]', err));
    gutil.log('[webpack]', stats.toString({
      colors: true,
      chunkModules: true
    }));
    next();
  });
}

task('build:dev', doWebpack.bind(null, deepAssign({
  output: {
    filename: 'redux-advanced-subscribe.js'
  }
}, baseWebpackCfg)));

task('build:min', doWebpack.bind(null, deepAssign({
  output: {
    filename: 'redux-advanced-subscribe.min.js'
  },
  plugins: [new ClosureCompilerPlugin({
    compiler: {
      language_in: 'ECMASCRIPT6',
      language_out: 'ECMASCRIPT5'
    },
    concurrency: 3
  })]
}, baseWebpackCfg)));
